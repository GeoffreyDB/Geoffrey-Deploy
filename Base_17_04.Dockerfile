FROM ubuntu:17.04

MAINTAINER Mario Zieschang <mziescha@cpan.org>

RUN     echo "deb http://old-releases.ubuntu.com/ubuntu/ zesty main restricted universe multiverse" > /etc/apt/sources.list \
    &&  echo "deb http://old-releases.ubuntu.com/ubuntu/ zesty-updates main restricted universe multiverse" >> /etc/apt/sources.list \
    &&  echo "deb http://old-releases.ubuntu.com/ubuntu/ zesty-security main restricted universe multiverse" >> /etc/apt/sources.list \
	&&  apt-get update; apt-get install -y perl g++ curl make libssl-dev \
    &&  curl -L https://cpanmin.us | perl - App::cpanminus \
    &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* \
    &&  cpanm --quiet DDP Test::CheckManifest Test::Pod::Coverage Pod::Usage \
    					Pod::Coverage::TrustPod Test::Pod List::MoreUtils::XS List::MoreUtils \
    					Test::Perl::Critic Devel::Cover::Report::Coveralls \
    &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*