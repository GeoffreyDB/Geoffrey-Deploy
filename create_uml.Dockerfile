FROM ubuntu:18.04

MAINTAINER Mario Zieschang <mziescha@cpan.org>

RUN         apt-get update && apt-get install -y perl g++ curl make graphviz \
        &&  curl -L https://cpanmin.us | perl - App::cpanminus \
        &&  cpanm -v DDP UML::Class::Simple File::Find::Rule File::Spec

COPY        create_uml.pl create_uml.pl
