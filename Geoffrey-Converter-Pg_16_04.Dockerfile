FROM mziescha/geoffrey_16_04

MAINTAINER Mario Zieschang <mziescha@cpan.org>

RUN         apt-get update && apt-get upgrade -y && apt-get install -y postgresql \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* \
        &&  mkdir -p /app

COPY        geoffrey/cpanfile /app/cpanfile

RUN         cpanm --quiet --installdeps /app && rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*

COPY        geoffrey /app

RUN         perl Makefile.PL \
        &&  make test RELEASE_TESTING=1 AUTHOR_TESTING=1 && cover -test -report html \
        &&  make install
