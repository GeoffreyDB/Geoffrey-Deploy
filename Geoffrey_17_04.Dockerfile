FROM mziescha/geoffrey-base_17_04

MAINTAINER Mario Zieschang <mziescha@cpan.org>

WORKDIR     /app

COPY        geoffrey/cpanfile /app/cpanfile

RUN         cpanm --quiet --installdeps /app && rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*

COPY        geoffrey /app

RUN         perl Makefile.PL \
        &&  make test RELEASE_TESTING=1 AUTHOR_TESTING=1 && cover -test -report html \
        &&  make install \
        &&  rm -rf /app/*
