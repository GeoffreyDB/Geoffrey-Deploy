#!/usr/bin/env perl

use DDP;
use File::Spec;
use UML::Class::Simple;
use File::Find::Rule;

my $s_package_name = $ARGV[0];
my $s_dir          = File::Spec->catfile( '..', $s_package_name, 'lib' );
my @files          = File::Find::Rule->file()->name('*.pm')->in($s_dir);

# produce a class diagram for your CPAN module on the disk
my @classes = classes_from_files( \@files );
my $painter = UML::Class::Simple->new( \@classes );

# ...and change the default title background color:
$painter->node_color('#ffffff');    # defaults to '#f1e1f4'

# hide all methods from parent classes
$painter->inherited_methods(0);
$painter->as_svg( File::Spec->catfile( '..', $s_package_name, lc($s_package_name) . '.svg' ) );
