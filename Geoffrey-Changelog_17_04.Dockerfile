FROM ubuntu:17.04

MAINTAINER Mario Zieschang <mziescha@cpan.org>

COPY        geoffrey/cpanfile /app/cpanfile

RUN         apt-get update && apt-get install -y perl g++ curl make libssl-dev \
        &&  curl -L https://cpanmin.us | perl - App::cpanminus \
        &&  cd /app \
        &&  cpanm --quiet DDP Devel::Cover Test::Perl::Critic Test::Pod \
        &&  cpanm --quiet --installdeps . && rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*
COPY        geoffrey /app
RUN         cd /app \
        &&  perl Makefile.PL \
        &&  make test RELEASE_TESTING=1 AUTHOR_TESTING=1 \
        &&  cover -test -report text \
        &&  make install \
        &&  rm -rf /app/*
